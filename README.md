# Overview

The simple java web application to simulate the advanced coffee maker.

# Deployment

The app is deployed using Heroku and can be accessed [here](https://coffeemaker-lieuvu.herokuapp.com/)
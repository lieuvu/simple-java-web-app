package com.coffeemaker;

public interface CoffeeMaker {
  public void click();

  public void addWater(int water);

  public void makeBoilingWater();
}

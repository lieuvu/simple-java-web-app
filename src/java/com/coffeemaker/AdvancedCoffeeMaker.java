package com.coffeemaker;

import java.util.Map;
import java.util.HashMap;

public class AdvancedCoffeeMaker implements CoffeeMaker {

    private boolean powerOn;
    private int arabicaLevel;
    private int waterLevel;
    private int milkLevel;
    private int cocoaLevel;
    private String msg;
    private static final int ARABICA_MAX_LEVEL = 2000;
    private static final int MILK_MAX_LEVEL = 2000;
    private static final int COCOA_MAX_LEVEL = 2000;
    private static final int WATER_MAX_LEVEL = 5000;

    /**
     * Create an instance of AdvancedCoffeeMaker.
     */
    private AdvancedCoffeeMaker() {
        powerOn = false;
        waterLevel = 0;
        arabicaLevel = 0;
        milkLevel = 0;
        cocoaLevel = 0;
        msg = "Waiting for action";
    }

    private static AdvancedCoffeeMaker instance = new AdvancedCoffeeMaker();

    /**
     * Get the singleton instane of AdvancedCoffeeMaker.
     */
    public static AdvancedCoffeeMaker getInstance() {
        return instance;
    }

    /**
     * Get the singleton instance of AdvancedCofeeMaker with the initial
     * ingredients level.
     *
     * @param powerOn       the boolean value to indicate the state of the
     *                      coffee machine - true is on, false is off
     * @param waterLevel    the level of water
     * @param arabicaLevel  the level of arabica
     * @param milkLevel     the level of milk
     * @param cocoaLevel    the level of cocoa
     */
    public static AdvancedCoffeeMaker getInstance(boolean powerOn,
        int waterLevel, int arabiaLevel, int milkLevel, int cocoaLevel) {
        instancepowerOn = powerOn;
        instancewaterLevel = waterLevel;
        instancearabicaLevel = arabiaLevel;
        instancemilkLevel = milkLevel;
        instancecocoaLevel = cocoaLevel;

        return instance;
    }

    /**
     * Click the power button of the coffee machine.
     */
    public void click() {
        powerOn = (!powerOn);
        msg = ("You turned the power " + isPowerOnStr());
    }

    /**
     * Add water to the coffee machine.
     *
     * @param water the water to add
     */
    public void addWater(int water) {
        if (waterLevel == WATER_MAX_LEVEL) {
          msg = "The water level is maximum. Do not need to add water.";
        }
        else {
            int newWaterLevel = waterLevel + water;

            if (newWaterLevel > WATER_MAX_LEVEL) {
                msg = "You added too much water. Max water level is: " +
                    WATER_MAX_LEVEL + "ml.";
            } else {
                waterLevel += water;
                msg = ("You added " + water + " ml of water. New water level:" + waterLevel + " ml.");
            }
        }
    }

    /**
     * Add arabica to the coffee machine.
     *
     * @param water the arabica to add
     */
    public void addArabica(int arabica) {
        if (arabicaLevel == ARABICA_MAX_LEVEL) {
            msg = "The espresso level is maximum. Do not need to add arabica.";
        }
        else {
            int newArabicaLevel = arabicaLevel + arabica;

            if (newArabicaLevel > ARABICA_MAX_LEVEL) {
                msg = "You added too much arabica. Max arabica level is: " +
                        ARABICA_MAX_LEVEL + "ml.";
            }
            else {
                arabicaLevel += arabica;
                msg = ("You added " + arabica + " ml of arabica. New arabica level:" + arabicaLevel + " ml.");
            }
        }
    }

    /**
     * Add milk to the coffee machine.
     *
     * @param milk the milk to add
     */
    public void addMilk(int milk) {
        if (milkLevel == MILK_MAX_LEVEL) {
            msg = "The milk level is maximum. Do not need to add milk.";
        }
        else {
            int newMilkLevel = milkLevel + milk;

            if (newMilkLevel > MILK_MAX_LEVEL) {
                msg = "You added too much milk. Max milk level is: " +
                    MILK_MAX_LEVEL + "ml.";
            }
            else {
                milkLevel += milk;
                msg = ("You added " + milk + " ml of milk. New milk level:" + milkLevel + " ml.");
            }
        }
    }

    /**
     * Add cocoa to the coffee machine.
     *
     * @param cocoa the cocoa to add
     */
    public void addCocoa(int cocoa) {
        if (cocoaLevel == COCOA_MAX_LEVEL) {
            msg = "The cocoa level is maximum. Do not need to add cocoa.";
        }
        else {
            int newCocoaLevel = cocoaLevel + cocoa;

            if (newCocoaLevel > COCOA_MAX_LEVEL) {
                msg = "You added too much cocoa. Max cocoa level is: " +
                    COCOA_MAX_LEVEL + "ml.";
            }
            else {
                cocoaLevel += cocoa;
                msg = ("You added " + cocoa + " ml of cocoa. New cocoa level:" + cocoaLevel + " ml.");
            }
        }
    }

    /**
     * Make boiling water.
     */
    public void makeBoilingWater() {
        Map<String, Integer> formula = new HashMap();

        formula.put("water", Integer.valueOf(150));

        if (waterLevel < ((Integer)formula.get("water")).intValue()) {
            msg = "Not Enough Water.";
        }
        else {
            waterLevel -= ((Integer)formula.get("water")).intValue();
            msg = "Make Boiling Water.";
        }
    }

    /**
     * Make espresso coffee.
     */
    public void makeEspressoCoffee() {
        Map<String, Integer> formula = new HashMap();

        formula.put("water", Integer.valueOf(10));
        formula.put("arabica", Integer.valueOf(80));

        if ( (waterLevel < ((Integer)formula.get("water")).intValue()) ||
             (arabicaLevel < ((Integer)formula.get("arabica")).intValue()) ) {
            msg = "\nNot Enough Espresso Coffee Ingredients.";
        }
        else {
            waterLevel -= ((Integer)formula.get("water")).intValue();
            arabicaLevel -= ((Integer)formula.get("arabica")).intValue();
            msg = "Make An Espresso Coffee.";
        }
    }

    /**
     * Make arabica coffee.
     */
    public void makeArabicaCoffee() {
        Map<String, Integer> formula = new java.util.HashMap();

        formula.put("water", Integer.valueOf(200));
        formula.put("arabica", Integer.valueOf(150));

        if ( (waterLevel < ((Integer)formula.get("water")).intValue()) ||
             (arabicaLevel < ((Integer)formula.get("arabica")).intValue()) ) {
            msg = "\nNot Enough Arabica Coffee Ingredients.";
        }
        else {
            waterLevel -= ((Integer)formula.get("water")).intValue();
            arabicaLevel -= ((Integer)formula.get("arabica")).intValue();
            msg = "\nMake An Arabia Coffee.";
        }
    }

    /**
     * Make cappuccino coffee.
     */
    public void makeCappuccino() {
        Map<String, Integer> formula = new java.util.HashMap();

        formula.put("arabica", Integer.valueOf(50));
        formula.put("milk", Integer.valueOf(150));
        formula.put("cocoa", Integer.valueOf(50));

        if ( (arabicaLevel < ((Integer)formula.get("arabica")).intValue()) ||
             (milkLevel < ((Integer)formula.get("milk")).intValue()) ||
             (cocoaLevel < ((Integer)formula.get("cocoa")).intValue()) ) {
            msg = "Not Enough Cappuccino Ingredients.";
        }
        else {
            arabicaLevel -= ((Integer)formula.get("arabica")).intValue();
            milkLevel -= ((Integer)formula.get("milk")).intValue();
            cocoaLevel -= ((Integer)formula.get("cocoa")).intValue();
            msg = "Make A Cappuccino.";
        }
    }

    /**
     * Make latte.
     */
    public void makeLatte() {
        Map<String, Integer> formula = new java.util.HashMap();

        formula.put("arabica", Integer.valueOf(20));
        formula.put("milk", Integer.valueOf(200));

        if ( (arabicaLevel < ((Integer)formula.get("arabica")).intValue()) ||
             (milkLevel < ((Integer)formula.get("milk")).intValue()) ) {
            msg = "Not Enough Latte Ingredients.";
        }
        else {
            arabicaLevel -= ((Integer)formula.get("arabica")).intValue();
            milkLevel -= ((Integer)formula.get("milk")).intValue();
            msg = "Make A Latte.";
        }
    }

    /**
     * Get the status of the coffee machin in string.
     *
     * @return "ON" if the power is on, otherwise "OFF"
     */
    public String isPowerOnStr() {
        if (powerOn) {
          return "ON";
        }

        return "OFF";
    }

    /**
     * Get arabica level.
     *
     * @return the arabica level of the coffee machine
     */
    public int getArabicaLevel() {
        return arabicaLevel;
    }

    /**
     * Get water level.
     *
     * @return the water level of the coffee machine
     */
    public int getWaterLevel() {
        return waterLevel;
    }

    /**
     * Get milk level.
     *
     * @return the milk level of the coffee machine
     */
    public int getMilkLevel() {
        return milkLevel;
    }

    /**
     * Get cocoa level.
     *
     * @return the cocoa level of the coffee machine
     */
    public int getCocoaLevel() {
        return cocoaLevel;
    }

    /**
     * Gets message the coffee machine will notify the user.
     *
     * @return msg the message to notify the user
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Checks if the power is on.
     *
     * @return true if the power is on, otherwise false
     */
    public boolean isPowerOn() {
        return powerOn;
    }

    /**
     * Sets the message for the coffee machine.
     */
    public void setMsg(String msg0) {
        msg = msg0;
    }
}

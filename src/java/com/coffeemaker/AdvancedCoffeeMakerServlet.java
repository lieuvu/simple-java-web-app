package com.coffeemaker;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdvancedCoffeeMakerServlet extends HttpServlet {

    private AdvancedCoffeeMaker advCoffeeMaker = AdvancedCoffeeMaker.getInstance();

    public AdvancedCoffeeMakerServlet() {}

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String coffeeMakerType = request.getParameter("type");
        String inputDataStr = request.getParameter("input");
        StringBuffer sb = new StringBuffer();

        if (coffeeMakerType.equals("advanced-coffee-maker")) {

            if (action.equals("load_page")) {
                advCoffeeMaker.setMsg("Waiting for action.");
            }
            else if (action.equals("click_power_btn")) {
                advCoffeeMaker.click();
            }
            else if (action.toLowerCase().contains("add")) {
                try {
                    int inputData = Integer.parseInt(inputDataStr);

                    if (inputData < 0) {
                        throw new NumberFormatException();
                    }

                    if (!advCoffeeMaker.isPowerOn()) {
                        advCoffeeMaker.setMsg("The Power is OFF. You must turn the Power ON to perform the task");
                    }
                    else {
                        switch (action) {
                            case "add_water":
                            advCoffeeMaker.addWater(inputData);
                            break;
                            case "add_arabica":
                            advCoffeeMaker.addArabica(inputData);
                            break;
                            case "add_cocoa":
                            advCoffeeMaker.addCocoa(inputData);
                            break;
                            case "add_milk":
                            advCoffeeMaker.addMilk(inputData);
                            break;
                            default:
                            advCoffeeMaker.setMsg("Error! Can not add ingredients!");
                        }
                    }
                }
                catch (NumberFormatException inputData) {
                    advCoffeeMaker.setMsg("The input is invalid. Please check again");
                }
            }
            else if (action.toLowerCase().contains("make")) {
                if (!advCoffeeMaker.isPowerOn()) {
                    advCoffeeMaker.setMsg("The Power is OFF. You must turn the Power ON to perform the task");
                }
                else {
                    switch (action) {
                          case "make_arabica_coffee":
                            advCoffeeMaker.makeArabicaCoffee();
                            break;
                          case "make_espresso_coffee":
                              advCoffeeMaker.makeEspressoCoffee();
                              break;
                          case "make_cappuccino":
                              advCoffeeMaker.makeCappuccino();
                              break;
                          case "make_latte":
                              advCoffeeMaker.makeLatte();
                              break;
                          case "make_boiling_water":
                              advCoffeeMaker.makeBoilingWater();
                              break;
                          default:
                            advCoffeeMaker.setMsg("Error! Can not make your coffee!");
                    }
                }
            }
        }

        sb.append("<advanced-coffee-machine>");
        sb.append("<power>" + advCoffeeMaker.isPowerOnStr() + "</power>");
        sb.append("<water>" + advCoffeeMaker.getWaterLevel() + "</water>");
        sb.append("<arabica>" + advCoffeeMaker.getArabicaLevel() + "</arabica>");
        sb.append("<milk>" + advCoffeeMaker.getMilkLevel() + "</milk>");
        sb.append("<cocoa>" + advCoffeeMaker.getCocoaLevel() + "</cocoa>");
        sb.append("<msg>" + advCoffeeMaker.getMsg() + "</msg>");
        sb.append("</advanced-coffee-machine>");
        response.setContentType("text/xml: charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(sb.toString());
    }

    public String getServletInfo() {
        return "Short description";
    }
}
